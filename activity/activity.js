//CREATE
db.users.insertMany(
    [
        {
            "firstName": "Green",
            "lastName": "Goblin",
            "email": "greengoblin@gmail.com",
            "password": "ilikegreen",
            "isAdmin": false
        },
        {
            "firstName": "Doc",
            "lastName": "Oc",
            "email": "dr.octopus@gmail.com",
            "password": "ilikeoctopus",
            "isAdmin": false
        },
        {
            "firstName": "Sand",
            "lastName": "Man",
            "email": "entersandman@gmail.com",
            "password": "ilikesand",
            "isAdmin": false
        },
        {
            "firstName": "Liz",
            "lastName": "Ard",
            "email": "lizman@gmail.com",
            "password": "ilikereptiles",
            "isAdmin": false
        },
        {
            "firstName": "Elec",
            "lastName": "Tro",
            "email": "electric.city@gmail.com",
            "password": "ilikeelectricity",
            "isAdmin": false
        }
    ]
)

db.courses.insertMany(
    [
        {
            "name": "HTML",
            "price": 2500,
            "isActive": false
        },
        {
            "name": "CSS",
            "price": 3000,
            "isActive": false
        },
        {
            "name": "JS",
            "price": 3500,
            "isActive": false
        }
    ]
)


//READ
db.users.find({"isAdmin": false});

//UPDATE
db.users.updateOne({},{$set:{"isAdmin":true}});

db.courses.updateOne({"name":"JS"},{$set:{"isActive":true}});


//DELETE
db.courses.deleteMany({"isActive": false})